function displayList(items, parent = document.body) {
    const ul = document.createElement('ul');

    for (let i = 0; i < items.length; i++) {
        const li = document.createElement('li');
        li.textContent = items[i];
        ul.appendChild(li);
    }

    parent.appendChild(ul);
}

const array1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
displayList(array1);


const parentElement = document.getElementById('myContainer');
displayList(parentElement);
